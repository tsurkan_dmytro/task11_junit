package epam.task.junits;

import org.junit.jupiter.api.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class MinesweeperTest {

    Minesweeper minesweeper;
    int M = Integer.parseInt("3");
    int N = Integer.parseInt("3");
    double p = Double.parseDouble("0.5");
    boolean[][] bombs = {{true,false,false,false},
                         {false,false,false,true},
                         {true,false,true,false},
                         {false,false,false,false}};


    @BeforeAll
    public static void setUp() throws Exception {

        System.out.println("Start MinesweeperTest");
    }

    @AfterAll
    public static void tearDown() throws Exception {
        System.out.println("End MinesweeperTest");
    }

    @Test
    public void printMapWithCounts() {
        minesweeper = new Minesweeper();
    }

    @Test
    public void findBomsItems() {
        minesweeper = new Minesweeper();
    }

    @Test
    @RepeatedTest(value = 5)
    public void printRealMap() {
        minesweeper = new Minesweeper();
        String expectedOutput  = "* . . . . . . * * . * . . . . . "; 

        // Do the actual assertion.
        assertEquals(expectedOutput, printRealMapTest(M, N, bombs));
    }

    @Test
    public void setBombsMap() {
        int counterTrue = 0;
        minesweeper = new Minesweeper();
        boolean[][] bombs = new boolean[M + 2][N + 2];
        minesweeper.setBombsMap(M, N, p, bombs);
        for (int i = 1; i <= M; i++)
            for (int j = 1; j <= N; j++) {
                if(bombs[i][j] == true){
                    counterTrue++;
                };
            }
        assertNotEquals(counterTrue,18);
    }

    public String printRealMapTest(int m, int n, boolean[][] bombs) {
        String test = "";
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++)
                if (bombs[i][j]) test += "* ";
                else             test += ". ";
            System.out.println();
        }
        return test;
    }

    @Test
    public void getBooldata() {
        minesweeper = new Minesweeper();
        assertTrue(minesweeper.getBoolData());
        assertFalse(minesweeper.getBoolData());
    }

    @Test
    public void getEmtyMinesweeper() {
        minesweeper = new Minesweeper();
        assertNull(minesweeper.getEmtyMineswwper());
        assertNotNull(minesweeper.getEmtyMineswwper());
    }
}