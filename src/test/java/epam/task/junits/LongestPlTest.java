package epam.task.junits;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongestPlTest {

    @BeforeAll
    public void setUp() throws Exception {

        System.out.println("Start LongestPlTest");
    }

    @AfterAll
    public void tearDown() throws Exception {
        System.out.println("End test LongestPi");
    }

    @Test
    public void findLongestConseqSubseq() {
        int result = 5;
        LongestPl longestPl = new LongestPl();
        int testArray[] =  {11, 14, 12, 13, 2, 6, 5, 88, 15, 7, 74, 33 };
        longestPl.findLongestConseqSubseq(testArray,12);
        assertEquals(longestPl.findLongestConseqSubseq(testArray,12), result);
    }
}