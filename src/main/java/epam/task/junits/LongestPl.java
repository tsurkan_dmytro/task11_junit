package epam.task.junits;

import java.util.HashSet;

public class LongestPl {

    public int findLongestConseqSubseq(int arr[],int n)
    {
        HashSet<Integer> integers = new HashSet<Integer>();
        int ans = 0;

        // Hash all the array elements
        for (int i = 0; i < n; ++i)
            integers.add(arr[i]);

        // check each possible sequence from the start
        // then update optimal length
        for (int i = 0; i < n; ++i)
        {
            if (!integers.contains(arr[i] - 1))
            {
                // Then check for next elements in the
                // sequence
                int j = arr[i];
                while (integers.contains(j))
                    j++;

                // update  optimal length if this length
                // is more
                if (ans < j - arr[i])
                    ans = j - arr[i];
            }
        }
        return ans;
    }
    public void showRwsult(){
        LongestPl longestPl = new LongestPl();
        int arr[] =  {9, 3, 10, 4, 20, 2, 45, 43, 44, 12, 18, 21,5 };
        int n = arr.length;
            System.out.println("Length of the Longest consecutive subsequence is " +
                    longestPl.findLongestConseqSubseq(arr,n));
    }
}
