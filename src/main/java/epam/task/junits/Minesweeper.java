package epam.task.junits;

public class Minesweeper {

    public void showRWsult() {
        int M = Integer.parseInt("10");
        int N = Integer.parseInt("34");
        double p = Double.parseDouble("0.3");

        // random bomb places
        boolean[][] bombs = new boolean[M + 2][N + 2];

        setBombsMap(M, N, p, bombs);

        // print real map
        printRealMap(M, N, bombs);

        // sol[i][j] = # bombs adjacent to cell (i, j)
        int[][] sol = new int[M + 2][N + 2];
        findBomsItems(M, N, bombs, sol);

        // print result
        printMapWithCounts(M, N, bombs, sol);
    }

    public void setBombsMap(int m, int n, double p, boolean[][] bombs) {
        for (int i = 1; i <= m; i++)
            for (int j = 1; j <= n; j++) {
                bombs[i][j] = (Math.random() < p);
            }
    }

    public void printRealMap(int m, int n, boolean[][] bombs) {
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++)
                if (bombs[i][j]) System.out.print("* ");
                else             System.out.print(". ");
            System.out.println();
        }
    }

    public void findBomsItems(int m, int n, boolean[][] bombs, int[][] sol) {
        for (int i = 1; i <= m; i++)
            for (int j = 1; j <= n; j++)
                // (ii, jj) indexes neighboring cells
                for (int ii = i - 1; ii <= i + 1; ii++)
                    for (int jj = j - 1; jj <= j + 1; jj++)
                        if (bombs[ii][jj]) sol[i][j]++;
    }

    public void printMapWithCounts(int m, int n, boolean[][] bombs, int[][] sol) {
        System.out.println();
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++)
                if (bombs[i][j]) System.out.print("* ");
                else System.out.print(sol[i][j] + " ");
            System.out.println();
        }
    }

    public boolean getBoolData(){
        return true;
    }
    
    public Minesweeper getEmtyMineswwper(){
        Minesweeper minesweeper = null;
        return minesweeper;
    }


}
